'use strict';
let Alexa = require("alexa-sdk");
let AWS = require('aws-sdk');
let topic_id = 'dogFeederTopic/feed'; 
let iotdata = new AWS.IotData({endpoint: 'a2e3kl20ixhlz.iot.us-east-1.amazonaws.com'});
/*
AWS.config.update({
    region: "us-east-1"
});
*/


exports.handler = function(event, context, callback) {
	// Lambda code goes here
	var alexa = Alexa.handler(event, context);
	
    alexa.registerHandlers(handlers);
    alexa.execute();
};

var handlers = {
    'LaunchRequest': function () {
		console.log('****LaunchRequest Handler ');
        this.emit(":tell", 'OK, I\'ll feed the dog.');
		feed_pet(this);
    },
	'FeedPet': function () {
		console.log('****FeedPet Handler ');
		feed_pet(this);
    },
};

// 3. Helper Function  =================================================================================================

function feed_pet(context){
	var quantity = '1'; 
	console.log('****feeding the dog '+ quantity + ' ounces');
	var speech_output = 'OK, I fed the dog '  + quantity + ' ounces';
	var payload = JSON.stringify({'quantity': quantity});
	//context.emit(":tell", speech_output);
	publishMessage(topic_id, payload, function (data){
		if (data === 'error') {
	    	console.log('****error: '+ payload + ' ounces');
			context.emit(":tell", 'the request failed');
		}  //an error occurred
			
		else {
			context.emit(":tell", speech_output);
		} 		
	});
	
}


//AWS IOT Call to publish a message on a provided topic
function publishMessage(topic, payload, eventCallback){
	// set region if not set (as not set by the SDK by default)
	if (!AWS.config.region) {
	  AWS.config.update({
		region: 'eu-east-1'
	  });
	}
	
    console.log('****publishMessage **** ');
	var params = {
		topic: topic, // required 
		payload: payload,
		qos: 1
	};
	
	console.log('****publishMessage:: Params ready:: ****' + payload);
	iotdata.publish(params, function(err, data) {
	if (err) {
		var error_string = JSON.stringify(err, null, 4)
		console.log('****feed_pet error:: '+ error_string, err.stack);
		eventCallback('error');
	}  //an error occurred
		
	else {
		var data_string = JSON.stringify(data, null, 4)
		console.log('****feed_pet - data:: '+data_string);
		eventCallback(data);
	} 

});


}
